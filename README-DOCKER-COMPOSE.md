# JHipster generated Docker-Compose configuration

## Usage

WARNING! Docker Compose configuration generated, but no Jib cache found
If you forgot to generate the Docker image for this application, please run:
To generate the missing Docker image(s), please run:
  ./gradlew bootJar -Pprod jibDockerBuild in /Users/gpienkowski/Dev/aps/gateway
  ./gradlew bootJar -Pprod jibDockerBuild in /Users/gpienkowski/Dev/aps/sensora
  ./gradlew bootJar -Pprod jibDockerBuild in /Users/gpienkowski/Dev/aps/sensorb

You can launch all your infrastructure by running : docker-compose up -d

Launch all your infrastructure by running: `docker-compose up -d`.

## Configured Docker services

### Service registry and configuration server:

- [JHipster Registry](http://localhost:8761)

### Applications and dependencies:

- gateway (gateway application)
- gateway's mongodb database
- sensora (microservice application)
- sensora's mongodb database
- sensorb (microservice application)
- sensorb's mongodb database

### Additional Services:

- Kafka
- Zookeeper
- [JHipster Console](http://localhost:5601)
- [Keycloak server](http://localhost:9080)
